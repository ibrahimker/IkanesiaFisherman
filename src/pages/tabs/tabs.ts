import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { Test1 } from '../test1/test1';
import { ProfileModalPage } from '../profile-modal/profile-modal';
import { ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProfileModalPage;
  //cartList=[];
  constructor(public cartModalCtrl : ModalController, public shoppingModalCtrl : ModalController, public profileModalCtrl : ModalController, public global: GlobalProvider, public navCtrl: NavController) {

  }

  public updateCartList(cartList,cartCounter) {
    console.log("Changed cart list",cartList)
    // this.global.cartList = cartList
    // this.global.cartCounter = cartCounter
    // x=this.global.cartCounter;
    console.log(this.global.cartCounter)
  }

  public openProfileModal(){
    var profileModalPage = this.profileModalCtrl.create('ProfileModalPage');
    profileModalPage.present();
  }
}

