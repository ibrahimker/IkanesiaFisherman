import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ShoppingModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shopping-modal',
  templateUrl: 'shopping-modal.html',
})
export class ShoppingModalPage {

  constructor(public navCtrl: NavController, public viewCtrl : ViewController, public navParams: NavParams, public http: Http, public global: GlobalProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShoppingModalPage');
  }

  public closeModal(){
	this.viewCtrl.dismiss();
  }

  public deliveryConfirmation(listIndex) {
  	this.global.cartListCheckout[listIndex].orderStatus=4;
  }
}
