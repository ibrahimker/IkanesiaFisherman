import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingModalPage } from './shopping-modal';

@NgModule({
  declarations: [
    ShoppingModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppingModalPage),
  ],
})
export class ShoppingModalPageModule {}
