import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { Geolocation } from '@ionic-native/geolocation';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

/**
 * Generated class for the ProfileModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-modal',
  templateUrl: 'profile-modal.html',
})
export class ProfileModalPage {

  loginText: boolean = false;
  email: string = '';
  pass: string = null;
  updateEmail: string = null;
  name: string = null;
  phone: string = null;
  address: string = null;
  edit: boolean = false;

  constructor(public geolocation: Geolocation, public navCtrl: NavController, public viewCtrl : ViewController, public navParams: NavParams, public http: Http, public global: GlobalProvider, public formBuilder: FormBuilder) {
	this.loginForm = formBuilder.group({
        emailAdd: ['', Validators.required],
        password: ['', Validators.required],
    });

    this.emailAdd = this.loginForm.controls['emailAdd'];
	this.password = this.loginForm.controls['password'];

	this.editForm = formBuilder.group({
        editName: [''],
        editEmail: [''],
        editPhone: [''],
        editAddress: ['']
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileModalPage');
  }

  public closeModal(){
	this.viewCtrl.dismiss();
  }

  public logout() {
    this.global.regQ = 'Terima kasih, untuk melanjutkan proses pembelian, mohon masukkan data Anda. Boleh kami tahu nama Anda?';
	this.global.regP = 'Nama lengkap Anda.';
	this.global.regN = 'Mohon maaf, nama Anda diperlukan untuk melakukan pembayaran.';
  	this.global.fullname = null;
  	this.global.custEmailAdd = null;
  	this.global.checkout = false;
  	this.global.counter0 = 0;
  	this.global.custName = null;
  	this.global.custEmail = null;
  	this.global.custPwd1 = null;
  	this.global.custPwd2 = null;
  	this.global.custRand1 = 0;
 	this.global.custRand2 = 0;
	this.global.inputName = null;
	this.global.regButton = 'KIRIM';
	this.global.regSuccess = null;
	this.global.done = false;
	this.global.chatList = [];
	this.global.cartCounter = null;
	this.global.cartList = []
	this.global.cartListCheckout = []
	this.global.chatList.push({
      type:'Pesan',
      text:"Selamat datang di IKANESIA! Ayo beli ikan kami, ikannya langsung dari nelayan. Baru saja ditangkap dan masih sangat segar loh!"
    })
    this.getCurrentPosition()
  }

  public registration() {
  	this.closeModal();
  	this.global.chatList.push({
    	type : 'Registration',
        text : "Hai nelayan, Anda harus menjadi anggota IKANESIA terlebih dahulu untuk melanjutkan proses penjualan. Boleh saya tahu nama anda?"})
  }

  public login() {
  	if(this.email == "abc@gmail.com" && this.pass == "password") {
  		this.global.fullname = "abc";
		this.global.custEmailAdd = this.email;
		this.closeModal();
  	}
  	else {
  		this.loginText = true;
  	}
  }

  public editData() {
  	this.edit = true;
  }

  public saveData(update) {
  	if(update) {
  		if(this.name != null) {
  			this.global.fullname = this.name;
  		}
  		if(this.updateEmail != null) {
  			this.global.custEmailAddress = this.updateEmail;
  		}
  		if(this.phone != null) {
  			this.global.custPhone = this.phone;
  		}
  		if(this.address != null) {
  			this.global.custAddress = this.address;
  		}
  		this.edit = false;
  	}
  	else if(!update) {
  		this.edit = false;
  	}
  }

  public getCurrentPosition(){
    this.geolocation.getCurrentPosition({timeout:500}).then((resp) => {
      var geocoder = new google.maps.Geocoder()
      var latlng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude)
      let chatListScoped = this.global.chatList
      var that = this
      geocoder.geocode({'location': latlng}, function(results, status) {
        if(status == "OK"){
          if(results[0]){
            chatListScoped.push({
              type:'Pesan',
              text:"Kamu sedang ada di " + results[0].address_components[4].short_name + ", " + results[0].address_components[5].short_name + ". Berikut beberapa ikan rekomendasi kami untuk anda"
            })
            setTimeout(function(){
              that.getRecommendedFish()
            }, 2000);
          }
        }
        else{
          console.log('Status is not OK', status);
          chatListScoped.push({
            type:'Pesan',
            text:"Kami tidak dapat mendeteksi lokasi Anda. Berikut beberapa ikan rekomendasi kami untuk Anda bedasarkan lokasi default kami."
          })
          setTimeout(function(){
            that.getRecommendedFish()
          }, 2000);
        }
      })
    }).catch((error) => {
      console.log('Error getting location', error);
      this.addChat("Kami tidak dapat mendeteksi lokasi Anda. Berikut beberapa ikan rekomendasi kami untuk Anda bedasarkan lokasi default kami.")
      var that = this
      setTimeout(function(){
        that.getRecommendedFish()
      }, 2000)
    })
  }

  public addChat(text){
    this.global.chatList.push({
      type:'Pesan',
      text:text
    })
  }

  public getRecommendedFish(){
    this.http.get("http://www.mocky.io/v2/5adfefa73300006200e4dab7").map(res => res.json()).subscribe(data => {
      Object.keys(data).forEach(key=> {
        this.addIkanToChat(data[key])
      });
    })
  }

  public addIkanToChat(dataIkan) {
    var waktuTangkapDalamJam = this.getTimeDifference(new Date(),new Date(dataIkan.waktuTangkap))
    this.global.chatList.push(
    {
      type:'Ikan',
      idIkan:dataIkan.id,
      namaIkan:dataIkan.nama,
      hargaIkan:dataIkan.harga,
      waktuTangkapanIkan:waktuTangkapDalamJam,
      namaTpi:dataIkan.namaTpi,
      idTpi:dataIkan.idTpi,
      latTpi:dataIkan.latTpi,
      longTpi:dataIkan.longTpi,
      stokIkan:dataIkan.stok,
      gambarIkan:dataIkan.img,
    })
  }

  public getTimeDifference(currentDate,catchFishDate){
    if (catchFishDate < currentDate) {
      catchFishDate.setDate(catchFishDate.getDate() + 1);
    }
    var diff = catchFishDate - currentDate;
    var msec = diff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    return Math.abs(hh)
  }
}
