import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {

	counter: integer = 0;
	testone: string = 'test';
	custName: string = 'hello';
	custEmail: string = 'email';
	custPwd1: string = 'pwd1';
	custPwd2: string = 'pwd2';
	custRand1: integer = 0;
	custRand2: integer = 0;
	inputName: string = null;
	regButton: string = 'KIRIM';
	regSuccess: boolean = null;

	formGroup: FormGroup;
	regInput: AbstractControl;

	regQ: string = 'Terima kasih, untuk melanjutkan proses pembelian, mohon masukkan data Anda. Boleh kami tahu nama Anda?';
	regP: string = 'Masukkan nama lengkap Anda.';
	regN: string = 'Mohon maaf, nama Anda diperlukan untuk melakukan pembayaran.';
	
	regQ1: string = 'senang berkenalan dengan Anda. Mohon masukkan email Anda.';
	regP1: string = 'Masukkan email Anda.';
	regN1: string = 'Mohon maaf, email Anda diperlukan untuk melakukan pembayaran.';

	regQ2: string = 'Email berhasil terverifikasi! Mohon masukkan kata sandi Anda.';
	regP2: string = 'Masukkan kata sandi.';
	regN2: string = 'Mohon maaf, kata sandi diperlukan untuk melakukan verifikasi data Anda.';

	regQ3: string = 'Terima kasih, mohon masukkan kembali kata sandi Anda.';
	regP3: string = 'Masukkan kata sandi.';
	regN3: string = 'Mohon maaf, mohon masukkan kembali kata sandi Anda untuk konfirmasi.';

	regQ4: string = 'Kami sudah mengirimkan kode verifikasi ke email Anda. Mohon, masukkan 4 digit kode tersebut di bawah.';
	regP4: string = 'Masukkan 4 digit kode verifikasi.';
	regN4: string = 'Mohon maaf, kode verifikasi yang Anda masukkan salah. Masukkan kembali 4 digit kode verifikasi Anda.';

	regQ5: string = 'Verifikasi berhasil! Terima kasih Anda telah berhasil menjadi anggota IKANESIA!';

	regQ6: string = 'Konfirmasi password gagal! Password yang Anda masukkan tidak sesuai dengan sebelumnya, mohon coba lagi.';
	regP6: string = 'Masukkan kata sandi.';
	regN6: string = 'Mohon maaf, mohon masukkan kembali kata sandi Anda untuk konfirmasi.';

	regQ7: string = 'Verifikasi Anda gagal! Mohon masukkan kode verifikasi yang benar.';
	regP7: string = 'Masukkan 4 digit kode verifikasi.';
	regN7: string = 'Mohon maaf, kode verifikasi yang Anda masukkan salah. Masukkan kembali 4 digit kode verifikasi Anda.';

	custReg () {
		
		this.counter0++;

		if (this.counter0 == 1) {
			this.regQ = 'Hai ' + this.inputName.toUpperCase() + ' ' + this.regQ1;
			this.custName = this.inputName;
			this.inputName = '';

			this.regP = this.regP1;
			this.regN = this.regN1;
		}
		else if (this.counter0 == 2) {

			if (new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$').test(this.inputName)) {

				this.regQ = this.regQ2;
				this.custEmail = this.inputName;
				this.inputName = '';

				this.regP = this.regP2;
				this.regN = this.regN2;

			}
			else {

				this.counter0 = 1;
				this.regQ = 'Maaf, email yang Anda masukkan salah, mohon masukkan kembali email Anda.';

			}


		}
		else if (this.counter0 == 3) {
			this.regQ = this.regQ3;
			this.custPwd1 = this.inputName;
			this.inputName = '';

			this.regP = this.regP3;
			this.regN = this.regN3;
		}
		else if (this.counter0 == 4) {
			this.regQ = this.regQ4;
			this.custPwd2 = this.inputName;
			this.inputName = '';

			if (this.custPwd1 == this.custPwd2) {

				this.regP = this.regP4;
				this.regN = this.regN4;

				this.custRand1 = Math.floor(Math.random() * (9999 - 1000)) + 1000;
				this.counter0 = 4;
			}
			else {

				this.regQ = this.regQ6;
				this.regP = this.regP6;
				this.regN = this.regN6;

				this.counter0 = 3;

			}
		}
		else if (this.counter0 >= 5) {
			this.custRand2 = this.inputName;
			this.inputName = '';

			this.regSuccess = false;

			if (this.custRand1 == this.custRand2) {

				this.regQ = this.regQ5;
				this.regP = this.regP5;
				this.regN = this.regN5;
				this.regButton = 'LANJUT PEMBAYARAN';

				this.regSuccess = true;
			}
			else {

				this.regQ = this.regQ7;
				this.regP = this.regP7;
				this.regN = this.regN7;

				this.regSuccess = false;
			}
		}

	}

	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {

		this.formGroup = formBuilder.group ({
			regInput:['', Validators.required]
			});

	this.regInput = this.formGroup.controls['regInput'];
	}

}