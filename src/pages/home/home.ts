import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Test1 } from '../test1/test1';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ModalController } from 'ionic-angular';
import { trigger,state,style,animate,transition,keyframes } from '@angular/animations';
import { GlobalProvider } from '../../providers/global/global';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
  trigger('flyInOut', [
    state('in', style({transform: 'translateX(0)'})),
    transition('void => *', [
      animate(300, keyframes([
        style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
        style({opacity: 1, transform: 'translateX(15px)',  offset: 0.3}),
        style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
        ]))
      ])
    ])
  ]
})

export class HomePage {
  cartCounter = 0
  chat=[{}]
  //chatList=[]
  ikan={}
  bankAccount : string = 'rekening bank BCA, atas nama PT. BELIKAN JAYA, no rek: 9750138190, cabang Sudirman';
  currentDate : string = new Date();

  formGroup: FormGroup;
  regInput: AbstractControl;
  accName: string = null;
  accNumber: string = null;
  orderNo: string = null;
  address: string = null;
  phone: string = null;
  
  regQ1: string = 'senang berkenalan dengan Anda. Mohon masukkan email Anda.';
  regP1: string = 'Masukkan email Anda.';
  regN1: string = 'Mohon maaf, email Anda diperlukan untuk melakukan pembayaran.';

  regQ2: string = 'Email berhasil terverifikasi! Mohon masukkan kata sandi Anda.';
  regP2: string = 'Masukkan kata sandi.';
  regN2: string = 'Mohon maaf, kata sandi diperlukan untuk melakukan verifikasi data Anda.';

  regQ3: string = 'Terima kasih, mohon masukkan kembali kata sandi Anda.';
  regP3: string = 'Masukkan kata sandi.';
  regN3: string = 'Mohon maaf, mohon masukkan kembali kata sandi Anda untuk konfirmasi.';

  regQ4: string = 'Kami sudah mengirimkan kode verifikasi ke email Anda. Mohon, masukkan 4 digit kode tersebut di bawah. Mohon untuk tidak menutup aplikasi ini.';
  regP4: string = 'Masukkan 4 digit kode verifikasi. Mohon untuk tidak menutup aplikasi ini.';
  regN4: string = 'Mohon maaf, kode verifikasi yang Anda masukkan salah. Masukkan kembali 4 digit kode verifikasi Anda. Mohon untuk tidak menutup aplikasi ini.';

  regQ5: string = 'Verifikasi berhasil! Terima kasih Anda telah berhasil menjadi anggota IKANESIA!';

  regQ6: string = 'Konfirmasi password gagal! Password yang Anda masukkan tidak sesuai dengan sebelumnya, mohon coba lagi.';
  regP6: string = 'Masukkan kata sandi.';
  regN6: string = 'Mohon maaf, mohon masukkan kembali kata sandi Anda untuk konfirmasi.';

  regQ7: string = 'Verifikasi Anda gagal! Mohon masukkan kode verifikasi yang benar.';
  regP7: string = 'Masukkan 4 digit kode verifikasi. Mohon untuk tidak menutup aplikasi ini.';
  regN7: string = 'Mohon maaf, kode verifikasi yang Anda masukkan salah. Masukkan kembali 4 digit kode verifikasi Anda. Mohon untuk tidak menutup aplikasi ini.';

// For Cart
  cartList=[]

  constructor(public geolocation: Geolocation, public cartModalCtrl : ModalController, public shoppingModalCtrl : ModalController, public http: Http, public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder){
    this.global.chatList.push({
      type:'Pesan',
      text:"Hai Nelayan, selamat datang di IKANESIA! Di sini kamu bisa menjual ikan-ikan hasil tangkapanmu langsung ke pelanggan dengan harga yang ok loh!"
    })
    this.global.chatList.push({
      type:'TombolAwal'
    })

    this.formGroup = formBuilder.group ({
      regInput:['', Validators.required]
    });

    this.regInput = this.formGroup.controls['regInput'];


    this.paymentUploadForm = formBuilder.group({
        accountName: [''],
        accountNumber: [''],
        orderNumber: ['']
    });

    this.addPhoneForm = formBuilder.group({
        addAddress: [''],
        addPhoneNo: [''],
        orderNumber: ['']
    });
  }


// Cart Related

  public addToCart(dataIkan) {
    var hargaTotalTpi = 0
    //If cartlist is empty, just direct push it
    if(this.global.cartList.length < 1) {
      hargaTotalTpi = dataIkan.hargaIkan
      this.addToCartList(false,0,dataIkan.namaTpi,dataIkan.idTpi,hargaTotalTpi,dataIkan.namaIkan,1,dataIkan.stokIkan,dataIkan.hargaIkan,dataIkan.hargaIkan)
      this.addChat("Ikan " + dataIkan.namaIkan.toUpperCase() + " sudah dimasukkan kedalam keranjang belanja Anda.")
      this.global.cartCounter++
    }
    else{ 
    //If cartlist is not empty, check whether ID TPI in the cartlist exists or not
      var isIdTpiExist = false
      var listIndexForExistTpi = 0
      for (var i = 0; i < this.global.cartList.length; i++) {
        if(this.global.cartList[i].idTpi == dataIkan.idTpi){
          isIdTpiExist = true
          listIndexForExistTpi = i 
          //Save the index into temp variable
          hargaTotalTpi = this.global.cartList[i].hargaTotalTpi //get already exist harga total
        }
      }
      if(isIdTpiExist){ 
      //If already exists, add only the data ikan
        hargaTotalTpi += dataIkan.hargaIkan
        this.addToCartList(true,listIndexForExistTpi,dataIkan.namaTpi,dataIkan.idTpi,hargaTotalTpi,dataIkan.namaIkan,1,dataIkan.stokIkan,dataIkan.hargaIkan,dataIkan.hargaIkan)
        this.addChat("Ikan " + dataIkan.namaIkan.toUpperCase() + " sudah dimasukkan kedalam keranjang belanja Anda.")
        this.global.cartCounter++
      }
      else{ 
      //If tpi doesn't exist in list, just direct push it
        hargaTotalTpi += dataIkan.hargaIkan
        this.addToCartList(false,0,dataIkan.namaTpi,dataIkan.idTpi,hargaTotalTpi,dataIkan.namaIkan,1,dataIkan.stokIkan,dataIkan.hargaIkan,dataIkan.hargaIkan)
        this.addChat("Ikan " + dataIkan.namaIkan.toUpperCase() + " sudah dimasukkan kedalam keranjang belanja Anda.")
        this.global.cartCounter++
      }
    }
  }


//Utility method untuk masukin ke cart list
  
  public addToCartList(isTpiExist,listIndex,namaTpi,idTpi,hargaTotalTpi,namaIkan,nominalIkan,stokIkan,hargaSatuan,hargaTotal) {
    if(isTpiExist) {
      this.global.cartList[listIndex].hargaTotalTpi = hargaTotalTpi
      this.global.cartList[listIndex].hargaTotalTpiIDR = Number(hargaTotalTpi).toLocaleString('id')
      this.global.cartList[listIndex].dataIkan.push({
        namaIkan:namaIkan,
        nominalIkan:nominalIkan,
        fishermanPhoNumber:"08123456789",
        stokIkan:stokIkan,
        hargaSatuan:hargaSatuan,
        hargaSatuanIDR: Number(hargaSatuan).toLocaleString('id'),
        hargaTotal:hargaTotal,
        hargaTotalIDR: Number(hargaTotal).toLocaleString('id')
      })
    }
    else {
      this.global.cartList.push({
        namaTpi:namaTpi,
        idTpi:idTpi,
        hargaTotalTpi:hargaTotalTpi,
        hargaTotalTpiIDR:Number(hargaTotalTpi).toLocaleString('id'),
        kurir:"Diambil Pembeli",
        orderNumber: null,
        orderStatus:0,
        timestamp: this.currentDate,
        dataIkan:[{
          namaIkan:namaIkan,
          nominalIkan:nominalIkan,
          fishermanPhoNumber:"08123456789",
          stokIkan:stokIkan,
          hargaSatuan:hargaSatuan,
          hargaSatuanIDR: Number(hargaSatuan).toLocaleString('id'),
          hargaTotal:hargaTotal,
          hargaTotalIDR: Number(hargaTotal).toLocaleString('id')
        }]
      })
    }
  }

  public updateCartList(cartList,cartCounter) {
    console.log("Changed cart list",cartList)
    // this.global.cartList = cartList
    // this.global.cartCounter = cartCounter
    // x=this.global.cartCounter;
    console.log(this.global.cartCounter)
  }

  public openCartModal(){
    var cartModalPage = this.cartModalCtrl.create('CartModalPage',{ counter:this.global.cartCounter,list : this.global.cartList , update:this.updateCartList.bind(this)});
    cartModalPage.present();
  }

  public isCartCounterShow(){
    if(this.global.cartCounter > 0 ) {
      return true
    }
    else {
      return false
    }
  }

//--End of Cart Related Functionality



//Chat Related Functionality
  
  public addChat(text){
    this.global.chatList.push({
      type:'Pesan',
      text:text
    })
  }

  public addIkanToChat(dataIkan) {
    var waktuTangkapDalamJam = this.getTimeDifference(new Date(),new Date(dataIkan.waktuTangkap))
    this.global.chatList.push(
    {
      type:'Ikan',
      idIkan:dataIkan.id,
      namaIkan:dataIkan.nama,
      hargaIkan:dataIkan.harga,
      hargaIkanIDR:Number(dataIkan.harga).toLocaleString('id'),
      waktuTangkapanIkan:waktuTangkapDalamJam,
      namaTpi:dataIkan.namaTpi,
      idTpi:dataIkan.idTpi,
      latTpi:dataIkan.latTpi,
      longTpi:dataIkan.longTpi,
      stokIkan:dataIkan.stok,
      gambarIkan:dataIkan.img,
    })
  }

//--End of Chat Related Functionality


//Utility Related Functionality
  
  public getTimeDifference(currentDate,catchFishDate){
    if (catchFishDate < currentDate) {
      catchFishDate.setDate(catchFishDate.getDate() + 1);
    }
    var diff = catchFishDate - currentDate;
    var msec = diff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    return Math.abs(hh)
  }
//--End of Utility Related Functionality


  public getRecommendedFish(){
    this.http.get("http://www.mocky.io/v2/5adfefa73300006200e4dab7").map(res => res.json()).subscribe(data => {
      Object.keys(data).forEach(key=> {
        this.addIkanToChat(data[key])
      });
    })
  }


//--Upload Payment Transfer Information
  
  public paymentUpload(){
  this.global.accName = null;
  this.global.accNumber = null;
  this.global.orderNo = null;
  this.global.paymentUpload = false;
    this.global.chatList.push({
        type : 'paymentUpload',
        text : "Mohon masukkan data konfirmasi pembayaran Anda sebagai berikut."
    })
  }

//--End of Payment Transfer Information

//--Add Customer Address and Phone Number

  public addPhone() {
    if(this.phone != null) {
      this.global.custPhone = this.phone;
    }
    if(this.address != null) {
      this.global.custAddress = this.address;
    }
    this.global.chatList.push({
        type : 'AddPhoneDone',
        text : "Terima kasih, Anda dapat melanjutkan pembayaran."
    })
  }

//--End of Add Customer Address and Phone Number

//--Payment Confirmation Information
  
  public paymentConfirmation(chatListIndex){
    this.global.accName = this.accName;
    this.global.accNumber = this.accNumber;
    this.global.orderNo = this.orderNo;
    this.accName = null;
    this.accNumber = null;
    this.orderNo = null;
    this.global.paymentUpload = true;
    this.global.chatList = this.remove(this.global.chatList,this.global.chatList[chatListIndex]);
    this.openShoppingModal();
    this.global.chatList.push({
        type : 'Pesan',
        text : "Terima kasih! Data Anda akan kami verifikasi dan kami perbaharui pada STATUS PEMESANAN. Selamat lanjutkan berbelanja!"
    })
  }

  public remove(array, element) {
  // Utility method to delete the array.

    return array.filter(e => e !== element)

  }

//--End of Payment Confirmation Information


//--Cart Status Modal Opening

  public openShoppingModal() {
    var shoppingModalPage = this.shoppingModalCtrl.create('ShoppingModalPage');
    shoppingModalPage.present();
  }

//--End of Cart Status Modal Opening



//--Registration

  public registration() {

    this.global.counter0++;

    if (this.global.counter0 == 1) {
      this.global.regQ = 'Hai ' + this.inputName.toUpperCase() + '' + this.regQ1;
      this.global.custName = this.inputName;
      this.inputName = '';

      this.global.regP = this.regP1;
      this.global.regN = this.regN1;
    }
    else if (this.global.counter0 == 2) {

      if (new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$').test(this.inputName)) {

        this.global.regQ = this.regQ2;
        this.global.custEmail = this.inputName;
        this.inputName = '';

        this.global.regP = this.regP2;
        this.global.regN = this.regN2;

      }
      else {

        this.global.counter0 = 1;
        this.global.regQ = 'Maaf, email yang Anda masukkan salah, mohon masukkan kembali email Anda.';

      }
    }
    else if (this.global.counter0 == 3) {
      this.global.regQ = this.regQ3;
      this.global.custPwd1 = this.inputName;
      this.inputName = '';

      this.global.regP = this.regP3;
      this.global.regN = this.regN3;
    }
    else if (this.global.counter0 == 4) {
      this.global.regQ = this.regQ4;
      this.global.custPwd2 = this.inputName;
      this.inputName = '';

      if (this.global.custPwd1 == this.global.custPwd2) {

        this.global.regP = this.regP4;
        this.global.regN = this.regN4;

        this.global.custRand1 = Math.floor(Math.random() * (9999 - 1000)) + 1000;
        this.global.counter0 = 4;
      }
      else {

        this.global.regQ = this.regQ6;
        this.global.regP = this.regP6;
        this.global.regN = this.regN6;

        this.global.counter0 = 3;

      }
    }
    else if (this.global.counter0 >= 5) {
      this.global.custRand2 = this.inputName;
      this.inputName = '';

      this.global.regSuccess = false;

      if (this.global.custRand1 == this.global.custRand2) {

        this.global.regQ = this.regQ5;
        this.global.regP = this.regP5;
        this.global.regN = this.regN5;
        if (this.global.checkout == true) {
          this.global.regButton = 'LANJUT PEMBAYARAN';
        }
        else {
          this.global.chatList.push({
            type : 'Pesan',
            text : "Terima kasih, selamat bergabung di IKANESIA! Sekarang Anda dapat mengakses semua fitur di IKANESIA."
          })
        }

        this.global.regSuccess = true;
        this.global.done = true;
        this.global.fullname = this.global.custName;
        this.global.custEmailAdd = this.global.custEmail;
      }
      else {

        this.global.regQ = this.regQ7;
        this.global.regP = this.regP7;
        this.global.regN = this.regN7;

        this.global.regSuccess = false;
      }
    }
  }
//--End of Registration

}
