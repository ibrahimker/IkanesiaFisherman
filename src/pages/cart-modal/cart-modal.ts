import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

@IonicPage()
@Component({
	selector: 'page-cart-modal',
	templateUrl: 'cart-modal.html',
})
export class CartModalPage {

	//cartList = []
	updateList:any
	//cartCounter = 1
	listKurir = ['Diambil Pembeli','Dikirim Pembeli', 'Diantar Penjual']
	//ongkir = 15000

	constructor(public navCtrl: NavController, public viewCtrl : ViewController, public profileModalCtrl : ModalController, public navParams: NavParams, public global: GlobalProvider) {

	}

	ionViewDidLoad() {
		//this.cartList = this.navParams.get('list')
		this.updateList = this.navParams.get('update')
		//this.cartCounter = this.navParams.get('counter')
	}

	public closeModal(){
		this.viewCtrl.dismiss();
		//console.log(this.ongkir)
	}

	public clearItem(listIndex,listIkanIndex){
	// To delete cart items.

		// If cartList == 1, set cartList == null.
	
		if(this.global.cartCounter == 1) {
			this.global.cartList = []
		}
		
		else { 

		// If cartList > 1, see which TPI has > 1 fish.
		
			if(this.global.cartList[listIndex].dataIkan.length>1) { 

			// If TPI has > 1 fish, delete the fish.
			
				this.global.cartList[listIndex].dataIkan = this.remove(this.global.cartList[listIndex].dataIkan,this.global.cartList[listIndex].dataIkan[listIkanIndex])
			}

			else { 

			// If TPI has 1 fish only, delete the TPI.
			
					this.global.cartList = this.remove(this.global.cartList,this.global.cartList[listIndex])
			}
		}

		if(this.global.cartCounter == 1) {
			this.global.cartList = []
			this.global.cartCounter = null
		}
		
		else { 
			this.global.cartCounter--
			this.updateCartList(this.global.cartList,this.global.cartCounter)
		}
	}

	updateCartList(list,counter){
		this.updateList(this.global.cartList,this.global.cartCounter)
	}


	public remove(array, element) {
	// Utility method to delete the array.

		return array.filter(e => e !== element)

	}

	public checkout (listIndex) {
		if (this.global.custAddress == null && this.global.custPhone == null) {
			this.global.chatList.push({
		      type : 'AddPhone',
		      text : 'Anda belum mengisi data alamat dan nomor telepon Anda, mohon lengkapi dahulu untuk dapat melakukan konfirmasi pembelian.'
		    })
		    this.closeModal ();
		}
		else if (this.global.custAddress == null) {
			this.global.chatList.push({
		      type : 'AddPhone',
		      text : 'Anda belum mengisi data alamat Anda, mohon lengkapi terlebih dahulu untuk dapat melakukan konfirmasi pembelian.'
		    })
		    this.closeModal ();
		}
		else if (this.global.custPhone == null) {
			this.global.chatList.push({
		      type : 'AddPhone',
		      text : 'Anda belum mengisi data nomor telepon Anda, mohon lengkapi terlebih dahulu untuk dapat melakukan konfirmasi pembelian.'
		    })
		    this.closeModal ();
		}
		else {
			this.closeModal ();

			if (this.global.fullname != null) {
				this.global.cartCounter = this.global.cartCounter - this.global.cartList[listIndex].dataIkan.length;
				if (this.global.cartCounter == 0) {
					this.global.cartCounter = null;
				}
				this.global.orderNumber++;
				this.global.cartList[listIndex].orderNumber = this.global.userID + this.global.orderNumber;
			    this.global.cartList[listIndex].orderStatus = 1;
			    this.global.cartList[listIndex].timestamp = new Date().toISOString();
			    this.global.cartListCheckout.push(this.global.cartList[listIndex]);
			    this.global.chatList.push({
			      type : 'paymentInfo',
			      text : this.global.cartList[listIndex].hargaTotalTpiIDR
			    })
				this.global.cartList = this.remove(this.global.cartList,this.global.cartList[listIndex])
			}
			else {
				this.openProfileModal();
				this.global.checkout = true;
			}
		}
	}

	public openProfileModal(){
	    var profileModalPage = this.profileModalCtrl.create('ProfileModalPage');
	    profileModalPage.present();
	}
}