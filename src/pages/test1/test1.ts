import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'test1-page',
  templateUrl: 'test1.html'
})
export class Test1 {

	constructor(public navCtrl: NavController) {

	}

}