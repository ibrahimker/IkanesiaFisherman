import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  public cartCounter = null;
  public cartList = []
  public cartListCheckout = []
  chatList = []
  public userID: string = '001';
  public orderNumber: integer = 0;
  public fullname = null;
  public custEmailAdd = null;
  public custPhone = null;
  public custAddress = null;
  public registration = null;
  public regButton: string = 'KIRIM';
  public checkout = false;
  public counter0 = 0
  public custName: string = null;
  public custEmail: string = null;
  public custPwd1: string = null;
  public custPwd2: string = null;
  public custRand1: integer = 0;
  public custRand2: integer = 0;
  public inputName: string = null;
  public regSuccess: boolean = null;
  public done: boolean = false;
  public regQ: string = 'Terima kasih, untuk melanjutkan proses pembelian, mohon masukkan data Anda. Boleh kami tahu nama Anda?';
  public regP: string = 'Nama lengkap Anda.';
  public regN: string = 'Mohon maaf, nama Anda diperlukan untuk melakukan pembayaran.';
  public accName: string = null;
  public accNumber: string = null;
  public orderNo: string = null;
  public paymentUpload: boolean = false;

  constructor(public http: HttpClient) {
    console.log('Hello GlobalProvider Provider');
  }
}
